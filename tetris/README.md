tetris - http://dbzard.bitbucket.org/tetris
=============

A simple tetris game build with Backbone. 一个使用Backbone.js编写的简单的俄罗斯方块游戏.

=============

![single play](https://bitbucket.org/dbzard/tetris/raw/ae2ecdebcd8a014ee7a77e85b9cb2cf10573de4a/screenshots/single.jpg)


![double play](https://bitbucket.org/dbzard/tetris/raw/ae2ecdebcd8a014ee7a77e85b9cb2cf10573de4a/screenshots/double.jpg)

=============

dbzard@gmail.com